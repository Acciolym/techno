from base import sql

mysql = sql.SQL("root", "root", "test")

comando = "DROP TABLE IF EXISTS tb_adm;"

if mysql.executar(comando, ()):
   print ("Tabela de administradores excluída com sucesso!")

comando = "DROP TABLE IF EXISTS tb_func;"

if mysql.executar(comando, ()):
   print ("Tabela de funcionários excluída com sucesso!")


comando = "CREATE TABLE tb_adm (idt_adm INT AUTO_INCREMENT PRIMARY KEY, " + \
         "nme_adm VARCHAR(50) NOT NULL, " + \
         "cpf_adm VARCHAR(11) NOT NULL , " + \
         "mat_adm VARCHAR(5) NOT NULL , " + \
         "senha_adm VARCHAR(10) NOT NULL);"

if mysql.executar(comando, ()):
   print ("Tabela de administradores criada com sucesso!")

comando = "CREATE TABLE tb_func (idt_func INT AUTO_INCREMENT PRIMARY KEY, " + \
          "nme_func VARCHAR(50) NOT NULL, " + \
          "cpf_func VARCHAR(11) NOT NULL, " + \
          "senha_func VARCHAR(10) NOT NULL ); "

if mysql.executar(comando, ()):
   print ("Tabela de funcionários criada com sucesso!")
