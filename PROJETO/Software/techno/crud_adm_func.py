import mysql.connector
import locale
from base import sql
from flask import Flask, render_template, request
app = Flask(__name__)

# URL: localhost:5000
@app.route('/')
def menu():
   return render_template('menu_adm_func.html')

# URL: localhost:5000/formincluir
@app.route('/formincluirAdm')
def formIncluirAdm():
   return render_template('formIncluir_adm.html')

@app.route('/incluirAdm', methods=['POST'])
def incluirAdm():
   # Recuperando dados do formulário de formIncluir()
   nome = request.form['nome']
   cpf = request.form['cpf']
   matricula = request.form['matricula']
   senha = request.form['senha']

   # Incluindo dados no SGBD
   mysql = sql.SQL("root", "root", "test")
   comando = "INSERT INTO tb_adm(nme_adm, cpf_adm, mat_adm, senha_adm) VALUES (%s, %s, %s, %s);"
   if mysql.executar(comando, [nome, cpf, matricula, senha]):
       msg="Administrador  " + nome + " cadastrado com sucesso!"
   else:
       msg="Falha no cadastro de Administrador."

   return render_template('incluir_adm.html', msg=msg)

@app.route('/paralterarAdm')
def parAlterarAdm():
   return render_template('parAlterar_adm.html')


@app.route('/formalterarAdm', methods=['POST'])
def formAlterarAdm():
    # Pegando os dados de parâmetro vindos do formulário parConsultar()
    nome = request.form['nome']

    # Recuperando modelos que satisfazem aos parâmetros de filtragem
    mysql = sql.SQL("root", "root", "test")
    comando = "SELECT * FROM tb_adm WHERE nme_adm=%s;"

    cs = mysql.consultar(comando, [nome])
    dados = cs.fetchone()
    cs.close()

    if dados == None:
        return render_template('naoEncontrado_adm.html')
    else:
        return render_template('formAlterar_adm.html', idt=dados[0], nome=dados[1], cpf=dados[2], matricula=dados[3],
                               senha=dados[4])


@app.route('/alterarAdm', methods=['POST'])
def alterarAdm():
   # Recuperando dados do formulário de formAlterar()
   idt = int(request.form['idt'])
   nome = request.form['nome']
   cpf = request.form['cpf']
   matricula = request.form['matricula']
   senha= request.form['senha']

   # Alterando dados no SGBD
   mysql = sql.SQL("root", "root", "test")
   comando = "UPDATE tb_adm SET nme_adm=%s, cpf_adm=%s, mat_adm=%s,senha_adm=%s WHERE idt_adm=%s;"

   if mysql.executar(comando, [ nome, cpf, matricula, senha, idt]):
       msg = "Administrador " + nome  + " alterado com sucesso!"
   else:
       msg = "Falha na alteração de dados."

   return render_template('alterar_adm.html', msg=msg)

@app.route('/parexcluirAdm')
def parExcluirAdm():
   # Recuperando todos os modelos da base de dados
   mysql = sql.SQL("root", "root", "test")
   comando = "SELECT idt_adm, nme_adm, cpf_adm, mat_adm FROM tb_adm ORDER BY nme_adm;"

   cs = mysql.consultar(comando, ())
   adms = ""
   for [idt, nome, cpf,  matricula] in cs:
       adms += "<TR>"
       adms += "<TD>" + nome + "</TD>"
       adms += "<TD>" + str(cpf) + "</TD>"
       adms += "<TD>" + str(matricula) + "</TD>"
       adms += "<TD><BUTTON ONCLICK=\"jsExcluir('" + nome + " (" + cpf + ")" + "', " + str(idt) + ")\">Excluir" + "</BUTTON></TD>"
       adms += "</TR>\n"
   cs.close()

   return render_template('parExcluir_adm.html', adms=adms)


@app.route('/excluirAdm', methods=['POST'])
def excluir():
   # Recuperando dados do formulário de parExcluir()
   idt = int(request.form['idt'])

   # Excluindo dados no SGBD
   mysql = sql.SQL("root", "root", "test")
   comando = "DELETE FROM tb_adm WHERE idt_adm=%s;"

   if mysql.executar(comando, [idt]):
       msg="Administrador  excluído com sucesso!"
   else:
       msg="Falha na exclusão de administrador."

   return render_template('excluir_adm.html', msg=msg)

@app.route('/parConsultarAdm')
def parConsultarAdm():
   # Recuperando todos os modelos da base de dados
   mysql = sql.SQL("root", "root", "test")
   comando = "SELECT idt_adm, nme_adm, cpf_adm, mat_adm FROM tb_adm ORDER BY nme_adm;"

   cs = mysql.consultar(comando, ())
   adms = ""
   for [idt, nome, cpf, matricula] in cs:
       adms += "<TR>"
       adms += "<TD>" + nome + "</TD>"
       adms += "<TD>" + str(cpf) + "</TD>"
       adms += "<TD>" + str(matricula) + "</TD>"
       adms += "</TR>\n"
   cs.close()

   return render_template('parConsultar_adm.html', adms=adms)


# URL: localhost:5000/formincluir
@app.route('/formincluirFunc')
def formIncluirFunc():
   return render_template('formIncluir_func.html')

@app.route('/incluirFunc', methods=['POST'])
def incluirFunc():
   # Recuperando dados do formulário de formIncluir()
   nome = request.form['nome']
   cpf = request.form['cpf']
   senha = request.form['senha']

   # Incluindo dados no SGBD
   mysql = sql.SQL("root", "root", "test")
   comando = "INSERT INTO tb_func(nme_func, cpf_func, senha_func) VALUES (%s, %s, %s);"
   if mysql.executar(comando, [nome, cpf, senha]):
       msg="Funcionário  " + nome + " cadastrado com sucesso!"
   else:
       msg="Falha no cadastro de Funcionário."

   return render_template('incluir_func.html', msg=msg)

@app.route('/paralterarFunc')
def parAlterarFunc():
   return render_template('parAlterar_func.html')


@app.route('/formalterarFunc', methods=['POST'])
def formAlterarFunc():
    # Pegando os dados de parâmetro vindos do formulário parConsultar()
    nome = request.form['nome']

    # Recuperando modelos que satisfazem aos parâmetros de filtragem
    mysql = sql.SQL("root", "root", "test")
    comando = "SELECT * FROM tb_func WHERE nme_func=%s;"

    cs = mysql.consultar(comando, [nome])
    dados = cs.fetchone()
    cs.close()

    if dados == None:
        return render_template('naoEncontrado_func.html')
    else:
        return render_template('formAlterar_func.html', idt=dados[0], nome=dados[1], cpf=dados[2],
                               senha=dados[3])


@app.route('/alterarFunc', methods=['POST'])
def alterarFunc():
   # Recuperando dados do formulário de formAlterar()
   idt = int(request.form['idt'])
   nome = request.form['nome']
   cpf = request.form['cpf']
   senha= request.form['senha']

   # Alterando dados no SGBD
   mysql = sql.SQL("root", "root", "test")
   comando = "UPDATE tb_func SET nme_func=%s, cpf_func=%s,senha_func=%s WHERE idt_func=%s;"

   if mysql.executar(comando, [ nome, cpf, senha, idt]):
       msg = "Funcionário " + nome  + " alterado com sucesso!"
   else:
       msg = "Falha na alteração de dados."

   return render_template('alterar_func.html', msg=msg)


@app.route('/parexcluirFunc')
def parExcluirFunc():
   # Recuperando todos os modelos da base de dados
   mysql = sql.SQL("root", "root", "test")
   comando = "SELECT idt_func, nme_func, cpf_func FROM tb_func ORDER BY nme_func;"

   cs = mysql.consultar(comando, ())
   funcs = ""
   for [idt, nome, cpf] in cs:
       funcs += "<TR>"
       funcs += "<TD>" + nome + "</TD>"
       funcs += "<TD>" + str(cpf) + "</TD>"
       funcs += "<TD><BUTTON ONCLICK=\"jsExcluir('" + nome + " (" + cpf + ")" + "', " + str(idt) + ")\">Excluir" + "</BUTTON></TD>"
       funcs += "</TR>\n"
   cs.close()

   return render_template('parExcluir_func.html', funcs=funcs)


@app.route('/excluirFunc', methods=['POST'])
def excluirFunc():
   # Recuperando dados do formulário de parExcluir()
   idt = int(request.form['idt'])

   # Excluindo dados no SGBD
   mysql = sql.SQL("root", "root", "test")
   comando = "DELETE FROM tb_func WHERE idt_func=%s;"

   if mysql.executar(comando, [idt]):
       msg="Funcionário  excluído com sucesso!"
   else:
       msg="Falha na exclusão de Funcionário."

   return render_template('excluir_func.html', msg=msg)

@app.route('/parConsultarFunc')
def parConsultarFunc():
   # Recuperando todos os modelos da base de dados
   mysql = sql.SQL("root", "root", "test")
   comando = "SELECT idt_func, nme_func, cpf_func FROM tb_func ORDER BY nme_func;"

   cs = mysql.consultar(comando, ())
   funcs = ""
   for [idt, nome, cpf] in cs:
       funcs += "<TR>"
       funcs += "<TD>" + nome + "</TD>"
       funcs += "<TD>" + str(cpf) + "</TD>"
       funcs += "</TR>\n"
   cs.close()

   return render_template('parConsultar_func.html',funcs=funcs)